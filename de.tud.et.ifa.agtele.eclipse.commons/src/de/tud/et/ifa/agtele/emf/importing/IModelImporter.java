package de.tud.et.ifa.agtele.emf.importing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.tud.et.ifa.agtele.emf.AgteleEcoreUtil;

public interface IModelImporter {

	IModelConnector getConnector();
	
	void setConnector(IModelConnector connector);
		
	default void importModel(Collection<EObject> target) {
		if (!this.getConnector().isConnected()) {
			this.getConnector().connect();
		}
		if (this.getConnector().isConnected()) {
					
			List<Object> rootNodes =  new ArrayList<>(this.findRootContentNodes());
			
			for (Object node : rootNodes) { //this cannot be parallelized due to the functionality of the import registry
				this.importRootContentNode(node, target);
			}	
			
			if (this.isSuppressParallelization()) {
				for (EObject obj : this.getCreatedEObjects()) {
					this.restoreAttributes(obj);
				}
				
				for (EObject obj : this.getCreatedEObjects()) {
					this.restoreReferences(obj);
				}
			} else {
				this.getCreatedEObjects().parallelStream().forEach(o -> this.restoreAttributes(o));
				this.getCreatedEObjects().parallelStream().forEach(o -> this.restoreReferences(o));
			}
			
			
			for (EObject obj : this.getCreatedEObjects()) {
				this.postImport(obj);
			}			
			this.getConnector().disconnect();
		}
	}
	
	default boolean isSuppressParallelization () {
		return this.getConnector().isSuppressParallelization();
	}

	default void importRootContentNode (Object node, Collection<EObject> target) {
		//set the context explicitly to null before importing determining the type of the next root node
		this.getImportRegistry().setContext(null);
		INodeDescriptor descriptor = node instanceof INodeDescriptor ? (INodeDescriptor) node : this.getConnector().getTypeInfo(node);
		if (descriptor != null) {
			EObject contentElement = this.createEObject(descriptor);
			target.add(contentElement);
			this.getImportRegistry().setContext(contentElement);
			this.getImportRegistry().registerImportedElement(node instanceof INodeDescriptor ? ((INodeDescriptor) node).getNode() : node, contentElement);
			this.importAllContents(contentElement);
		}
		this.getImportRegistry().setContext(null);
	}
	
	@SuppressWarnings("rawtypes")
	default void importAllContents (EObject element) {
		if (element == null) {
			return;
		}
		this.importContents(element);
		List<EObject> contents = new ArrayList<>();
		
		Iterator<EObject> it = element.eContents().iterator();
		while (it.hasNext()) {
			contents.add(it.next());
		}

		if (this.getConnector().getPotentialContentsLength(this.getOriginalNode(element)) > -1 && 
				contents.size() != this.getConnector().getPotentialContentsLength(this.getOriginalNode(element))) {
			synchronized (this) {
				INodeDescriptor nd = this.getConnector().getTypeInfo(this.getOriginalNode(element));
				System.err.println("Potentially invalid content length detected, model may not be reliable.");
				System.err.println("original node type: " + nd.getNsUri() + "#" + nd.getTypeName());
				System.err.println("original node: " + nd.getNode().toString());
				if (this.getOriginalNode(element) instanceof EObject) {
					System.err.println("path: " + this.getNamedPath((EObject)this.getOriginalNode(element)));
				}
//				for (EObject imported : contents) {
//					this.getImportRegistry().deregister(imported);
//				}
//				for (EReference ref : element.eClass().getEAllContainments()) {
//					if (ref.isMany()) {
//						((EList)element.eGet(ref)).clear();
//					} else {
//						element.eSet(ref, null);
//					}
//				}
//				contents.clear();
				this.importContents(element);
				it = element.eContents().iterator();
				while (it.hasNext()) {
					contents.add(it.next());
				}
			}
		}
		
		if (this.isSuppressParallelization() || AgteleEcoreUtil.getAllContainers(element).size() > 3) {
			for (int i = 0; i < contents.size(); i += 1) {
				try {
					this.importAllContents(contents.get(i));
				} catch (Exception e) {
					//Do something
					System.err.println("Error on restoring the " + i + "th content element of element '" + element.toString() + "' with uri '" + EcoreUtil.getURI(element) + "'");
					e.printStackTrace();
				}
			}			
		} else {
			contents.parallelStream().forEach(o -> {
				try {
					this.importAllContents(o);
				} catch (Exception e) {
					//Do something
					System.err.println("Error on restoring a content element of '" + element.toString() + "' with uri '" + EcoreUtil.getURI(element) + "', sry, don't know which element since the content is imported in a parallelized manner.");
					e.printStackTrace();
				}
			});
		}
	}
	
	default void init() {};
	
	IModelElementImportRegistry getImportRegistry();
	
	default EObject createEObject (INodeDescriptor descriptor) {
		EPackage.Registry registry = this.getEPackageRegistry();
		if (registry == null) {
			return null;
		}
		EPackage pkg = registry.getEPackage(descriptor.getNsUri());
		if (pkg == null) {
			return null;
		}
		EClassifier classifier = pkg.getEClassifier(descriptor.getTypeName());
		if (!(classifier instanceof EClass) || ((EClass)classifier).isAbstract()) {
			return null;
		}
		EObject obj = null;
		obj = pkg.getEFactoryInstance().create((EClass)classifier);
		
		this.getImportRegistry().registerImportedElement(descriptor.getNode(), obj);
		
		return obj;
	}
	
	default EObject getCreatedEObject (Object node) {
		return this.getImportRegistry().getImportedElement(node);
	}
	
	default Set<EObject> getCreatedEObjects (Object node) {
		return this.getImportRegistry().getImportedElements(node);
	}
	
	default Object getOriginalNode (EObject eObject) {
		return this.getImportRegistry().getOriginalElement(eObject);
	}

	default Collection<EObject> getCreatedEObjects() {
		return this.getImportRegistry().getImportedElements();
	}
			
	default void importContents(EObject eObject) {
		if (eObject == null) {
			return;
		}
		IModelImportStrategy strategy = this.getImportStrategy(eObject.eClass());
		for (EReference ref : strategy.getEContainmentsForImport(this, this.getConnector(), eObject)) {
			this.importContent(eObject, ref);
		}
	}
	
	default void restoreReferences(EObject eObject) {
		Collection<EReference> refs =  this.getImportStrategy(eObject.eClass()).getEReferencesForImport(this, this.getConnector(), eObject);
//		if (this.isSuppressParallelization()) {
			for (EReference ref : refs) {
				this.restoreReference(eObject, ref);
			}
//		} else {
//			refs.parallelStream().forEach(r -> {
//				this.restoreReference(eObject, r);
//			});
//		}		
	}
	
	@SuppressWarnings("rawtypes")
	default String getNamedPath(EObject obj) {
		String result = "";
		EObject current = obj;
		if (obj == null) {
			return result;
		}
		ContainerLoop:
		do {
			EStructuralFeature feat = current.eClass().getEStructuralFeature("name");
			if (feat instanceof EAttribute) {
				if (feat.isMany()) {
					for (Object val : (EList)current.eGet(feat)) {
						if (val instanceof String && !((String)val).isBlank()) {
							result = ((String) val)+" > "+result;
							continue ContainerLoop;
						}
					}
					result = "? > "+result;
				} else {
					Object val = current.eGet(feat);
					if (val instanceof String) {
						result = ((String) val)+" > "+result;
					} else {
						result = "? > "+result;
					}
				}
			}
			current = current.eContainer();
		} while (current != null);
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	default void restoreAttributes(EObject eObject) {
		Collection<EAttribute> attrs = this.getImportStrategy(eObject.eClass()).getEAttributesForImport(this, this.getConnector(), eObject);
		int restoredAttributesCount = 0;
		
//		if (this.isSuppressParallelization()) {
			for (EAttribute attr : attrs) {
				this.restoreAttribute(eObject, attr);
				if (attr.isMany()) {
//					if (attr.isUnique()) {
//						restoredAttributesCount += this.getConnector().readAttribute(this.getOriginalNode(eObject), attr).size();
//					} else {
						restoredAttributesCount += ((EList)eObject.eGet(attr)).size();
//					}
				} else if (eObject.eIsSet(attr)) {
					restoredAttributesCount += 1;
				} else {
					restoredAttributesCount += this.getConnector().readAttribute(this.getOriginalNode(eObject), attr).size();
				}
			}
			
			if (this.getConnector().getPotentialAttributesCount(this.getOriginalNode(eObject)) > -1 && 
					restoredAttributesCount != this.getConnector().getPotentialAttributesCount(this.getOriginalNode(eObject))) {
				synchronized (this) {
					INodeDescriptor nd = this.getConnector().getTypeInfo(this.getOriginalNode(eObject));
					System.err.println("Potentially invalid attributes count detected, model may not be reliable counted: " + 
							restoredAttributesCount + ", expected: " + this.getConnector().getPotentialAttributesCount(this.getOriginalNode(eObject)));
					System.err.println("    original node type: " + nd.getNsUri() + "#" + nd.getTypeName());
					System.err.println("    original node: " + nd.getNode().toString());
					System.err.println("    imported node: " + eObject.toString());
					if (this.getOriginalNode(eObject) instanceof EObject) {
						System.err.println("path: " + this.getNamedPath((EObject)this.getOriginalNode(eObject)));
					}
				}
			}			
//		} else {
//			attrs.parallelStream().forEach(a -> {
//				this.restoreAttribute(eObject, a);				
//			});
//		}
	}
	
	default void importContent(EObject eObject, EReference reference) {
		this.getImportStrategy(eObject.eClass(), reference)
			.importContent(this, this.getConnector(), eObject, reference);
	}

	default void restoreReference(EObject eObject, EReference reference) {
		this.getImportStrategy(eObject.eClass(), reference)
			.restoreReference(this, this.getConnector(), eObject, reference);
	}
	
	default void restoreAttribute(EObject eObject, EAttribute attribute) {
		this.getImportStrategy(eObject.eClass(), attribute)
			.restoreAttribute(this, this.getConnector(), eObject, attribute);
	}
	
	default void postImport(EObject eObject) {
		this.getImportStrategy(eObject.eClass()).postImport(this, this.getConnector(), eObject);
	}
	
	default Set<Object> findRootContentNodes() {
		Set<Object> result = new HashSet<>();
		Collection<Object> content = this.getConnector().browse(null);
		if (content != null && !content.isEmpty()) {
			for (Object obj : content) {
				if (this.getConnector().isValidRootSearchNode(obj)) {
					result.addAll(this.findRootContentNodes(obj));					
				}
			}
		}
		return result;
	}

	default Set<Object> findRootContentNodes(Object node) {		
		Set<Object> result = new HashSet<>();
		
		if (this.getConnector().getTypeInfo(node) != null) {
			result.add(node);
		} else {
			if (this.getConnector().isValidRootSearchNode(node)) {
				Collection<Object> content = this.getConnector().browse(node);
				for (Object obj : content) { //TODO parallelize
					result.addAll(this.findRootContentNodes(obj));
				}
			}
		}		
		return result;
	}
	
	final Map<EClass, IModelImportStrategy> globalImportStrategyRegistry = new HashMap<>();
			
	public static void registerGlobalImportStrategy(EClass cls, IModelImportStrategy strategy) {
		if (cls != null && strategy != null) {
			globalImportStrategyRegistry.put(cls, strategy);
		}
	}
	
	public static IModelImportStrategy getGlobalImportStrategy(EClass cls) {
		return globalImportStrategyRegistry.get(cls);
	}
	
	
	default IModelImportStrategy getImportStrategy(EClass cls) {
		if (this.getLocalImportStrategy(cls) != null) {
			return this.wrapImportStrategy(this.getLocalImportStrategy(cls));
		}
		if (IModelImporter.getGlobalImportStrategy(cls) != null) {
			return this.wrapImportStrategy(IModelImporter.getGlobalImportStrategy(cls));
		}
		return this.wrapImportStrategy(this.getDefaultImportStrategy());
	}
	
	default IModelImportStrategy getImportStrategy(EClass cls, EStructuralFeature feature) {
		if (this.getImportStrategy(feature.getEContainingClass()).unwrap()==this.getDefaultImportStrategy()) {
			return this.getImportStrategy(cls);
		}
		return this.getImportStrategy(feature.getEContainingClass());
	}

	default EPackage.Registry getEPackageRegistry() {
		return EPackage.Registry.INSTANCE;
	}
	
	public static final IModelImportStrategy DEFAULT_IMPORTSTRATEGY = new IModelImportStrategy() {};

	default IModelImportStrategy getDefaultImportStrategy() {
		return IModelImporter.DEFAULT_IMPORTSTRATEGY;
	}
		
	void setImportStrategyWrappers (IDelegatingModelImportStrategy[] wrappers);

	IDelegatingModelImportStrategy[] getImportStrategyWrappers();

	default IModelImportStrategy wrapImportStrategy(IModelImportStrategy strategy) {
		if (strategy == null) {
			return null;
		}
		IDelegatingModelImportStrategy[] wrappers = this.getImportStrategyWrappers();
		
		if (wrappers == null || wrappers.length == 0) {
			return strategy;		
		}
		IDelegatingModelImportStrategy currentStrategy = wrappers[wrappers.length -1].wrap(strategy);
		for (int i = wrappers.length - 2; i >= 0; i-=1) {
			if (currentStrategy == null) {
				break;
			}
			currentStrategy = wrappers[i].wrap(currentStrategy);
		}
		return currentStrategy;
	}
	//TODO isValidContentNode -> get Eclass from NodeDescriptor

	void registerLocalImportStrategy(EClass cls, IModelImportStrategy strategy);
	
	IModelImportStrategy getLocalImportStrategy(EClass cls);
}
