package de.tud.et.ifa.agtele.emf.importing;

public interface IImportRegistryProvider {
	public IModelElementImportRegistry getImportRegistry ();
}
