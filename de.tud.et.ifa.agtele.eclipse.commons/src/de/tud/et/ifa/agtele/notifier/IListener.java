package de.tud.et.ifa.agtele.notifier;

public interface IListener <NotificationType> {

	public void notify(NotificationType notification);
	
}
