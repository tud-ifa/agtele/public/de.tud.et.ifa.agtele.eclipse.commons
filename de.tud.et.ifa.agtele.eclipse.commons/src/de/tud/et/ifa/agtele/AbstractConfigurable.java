package de.tud.et.ifa.agtele;

import java.util.HashMap;
import java.util.Map;

public class AbstractConfigurable {
	protected Map<String, Object> config = new HashMap<>();
	
	public void configure(String key, Object value) {
		this.config.put(key, value);
	}
	
	public Map<String, Object> getConfig () {
		return new HashMap<>(this.config);
	}
	
	public String c (String key) {
		Object value = this.config.get(key);
		if (value instanceof String) {
			return (String) value;
		}
		if (value != null) {
			return value.toString();
		}
		return "";
	}
	public Integer ci (String key) {
		Object value = this.config.get(key);
		if (value instanceof Integer) {
			return (Integer) value;
		}
		return null;
	}
	public Boolean cb (String key) {
		Object value = this.config.get(key);
		if (value instanceof Boolean) {
			return (Boolean) value;
		}
		return Boolean.FALSE;
	}
}
