package de.tud.et.ifa.agtele.ui.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.swt.dnd.ByteArrayTransfer;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;

import de.tud.et.ifa.agtele.emf.AgteleEcoreUtil;

public class ReferencingIdentifierTransfer extends ByteArrayTransfer {

	public final static ReferencingIdentifierTransfer instance = new ReferencingIdentifierTransfer ();
	
	protected ReferencingIdentifierTransfer() {}

	public static ReferencingIdentifierTransfer getInstance () {
		return instance;
	}
	
	static final String MIME_TYPE= "text/referencing-identifier-list";
	final int MIME_TYPE_ID = registerType(MIME_TYPE);
	
	@Override
	protected int[] getTypeIds() {
		return new int[] {MIME_TYPE_ID};
	}

	@Override
	protected String[] getTypeNames() {
		return new String[] {MIME_TYPE};
	}
	
	public boolean validate(Object o) {
		if (o instanceof ReferencingIdentifierList) {
			return true;
		}
		if (!(o instanceof Collection)) {
			return false;
		}
		for (Object element : (Collection)o) {
			if (!(element instanceof String)) {
				return false;
			}
		}
		return true;
	}
	
	public String convertToText (Collection c) {
		if (this.validate(c)) {
			String text = "[";
			
			for (Object element : c) {
				text += "" + ((String)element).length() + "_" + ((String)element);
			}
			
			text += "]";
			
			if (c instanceof ReferencingIdentifierList) {
				text += this.appendMappings((ReferencingIdentifierList)c);
			}
			
			return text;
		}
		return null;
	}
	
	protected String appendMappings(ReferencingIdentifierList c) {
		String result = "";
		if (!c.getMappedIds().isEmpty()) {
			for (int i = 0; i < c.size(); i += 1) {
				String id = c.get(i);
				String mapped = c.getMappedId(id);
				if (mapped != null && !mapped.isBlank()) {
					result += i + "_" + mapped.length() + "_" + mapped;
				}				
			}
		}
		return result;
	}

	protected void parseMappings(String text, ReferencingIdentifierList result) {
		while (!text.isEmpty()) {
			int firstDelim = text.indexOf('_'),
				secondDelim = text.indexOf('_', firstDelim + 1);
			int mIdIndex = Integer.parseInt(text.substring(0, firstDelim)),
				mIdLength = Integer.parseInt(text.substring(firstDelim + 1, secondDelim));
			String mId = text.substring(secondDelim + 1, secondDelim + 1 + mIdLength);
			text = text.substring(secondDelim + 1 + mIdLength);
			
			result.registerMappedId(result.get(mIdIndex), mId);
		}
	}
	
	@Override
	protected void javaToNative (Object object, TransferData transferData) {
		if (this.validate(object)) {
			String text = convertToText((Collection) object);
			if (text != null) {
				byte[] bytes = text.getBytes(); 
				super.javaToNative(bytes, transferData);
			}
			
		}
	}
	
	public Collection<?> convertFromText (String text) {
		ReferencingIdentifierList result = new ReferencingIdentifierList();

		if (!text.startsWith("[") || !text.endsWith("]")) {
			return null;
		}
		
		text = text.substring(1, text.length()-1);
		
		if (text.isEmpty()) {
			return result;
		}
		while (!text.isEmpty()) {
			if (text.startsWith("]")) {
				text = text.substring(1, text.length()-1);
				break;
			}
			
			int delim = text.indexOf("_"),
				length = Integer.parseInt(text.substring(0, delim)); 
			result.add(text.substring(delim+1, delim+1+length));
			text = text.substring(delim+1+length);			
		}
		if (!text.isEmpty()) {
			this.parseMappings(text, result);
		}
		
		return Collections.singleton(result);
	}
	
	@Override
	public Object nativeToJava(TransferData transferData) {		
		if (transferData == null) {
			return null;
		}
		byte[] bytes = (byte[])super.nativeToJava(transferData);
		if (bytes == null) {
			return null;
		}
		String text = new String(bytes);
		if (text != null) {
			try {
				return this.convertFromText(text);
			} catch (Exception e) {
				System.err.println("Could not convert dnd-string: '" + text + "'");
			}
		}
		return null;
	}
	
	public static List<EObject> referencableObjects (Collection<? extends Object> objects) {
		ArrayList<EObject> result = new ArrayList<>();
		for (Object o : objects) {
			if (o instanceof EObject && AgteleEcoreUtil.isReferencable(o)) {
				result.add((EObject)o);
			}
		}
		return result;
	}
	
	public static boolean isFromOtherResource (Collection<?> objects, Resource res) {
		for (Object obj : objects) {
			if (obj instanceof EObject) {
				if (((EObject)obj).eResource() != res) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static class ReferencingIdentifierList extends ArrayList<String> {
		private static final long serialVersionUID = -977424907021486244L;
		
		protected Map<String, String> mappedId = new HashMap<>();
		
		public ReferencingIdentifierList() {
			super();
		}
		
		public ReferencingIdentifierList(Collection<String> strings) {
			super(strings);
		}
		
		public ReferencingIdentifierList(List<EObject> referencables) {
			super();
			
			for (EObject obj : referencables) {
				String id = this.getIdentifier(obj);
				if (id != null && !id.isBlank()) {
					this.add(id);
				}
			}
		}
		
		public String getIdentifier (EObject obj) {
			List<String> ids = AgteleEcoreUtil.getReferencableStrings(obj);			
			if (ids != null && !ids.isEmpty()) {
				return ids.get(0);
			}			
			return null;
		}
		
		public void registerMappedId (String original, String mapped) {
			this.mappedId.put(original, mapped);
		}
		
		public List<String> getMappedIds () {
			return new ArrayList<>(this.mappedId.values());
		}
		
		public String getMappedId(String original) {
			return this.mappedId.get(original);
		}
	}
}
