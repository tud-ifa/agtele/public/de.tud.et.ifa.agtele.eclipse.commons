package de.tud.et.ifa.agtele.ui.dialogs;

import java.util.Collection;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

public class WorkspaceElementSingleSelectionDialog {
	
	protected Object input = null;

	protected Shell parent = null;
	protected ElementTreeSelectionDialog dialog = null;
	
	protected ILabelProvider labelProvider = null;
	protected ITreeContentProvider contentProvider = null;
	
	public WorkspaceElementSingleSelectionDialog(Shell parent) {
		this.parent = parent;
		this.labelProvider = this.createLabelProvider();
		this.contentProvider = this.createContentProvider();
		
		this.dialog = new ElementTreeSelectionDialog(parent, labelProvider, contentProvider);
		
		setDefaultInput();
	}

	protected ITreeContentProvider createContentProvider() {
		return new  WorkbenchContentProvider() {
			@SuppressWarnings("rawtypes")
			@Override
			public Object[] getElements(Object element) {
				if (element == WorkspaceElementSingleSelectionDialog.this.input) {
					if (WorkspaceElementSingleSelectionDialog.this.input instanceof Object[]) {
						return (Object[]) WorkspaceElementSingleSelectionDialog.this.input;
					}
					if (WorkspaceElementSingleSelectionDialog.this.input instanceof Collection) {
						return ((Collection)WorkspaceElementSingleSelectionDialog.this.input).toArray();
					}
				}
				return super.getElements(element);
			}
		};
	}

	protected ILabelProvider createLabelProvider() {
		return new WorkbenchLabelProvider();
	}

	public void setDefaultInput() {
		try {
			this.input = ResourcesPlugin.getWorkspace().getRoot().members();
			this.dialog.setInput(this.input);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	public IResource getResult() {
		return (IResource) this.dialog.getFirstResult();
	}
	
	public int open () {
		return this.dialog.open();
	}
}
