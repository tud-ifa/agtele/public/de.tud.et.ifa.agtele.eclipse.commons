package de.tud.et.ifa.agtele.eclipse.webpage.generator.artifacts

import java.util.List

interface BootstrapHtmlGenerator extends BasicHtmlGenerator {
		
	override List<String> linkedStylesheets() {
		val list = BasicHtmlGenerator.super.linkedStylesheets;
		list.add(0,this.webPage.baseUrl+"/templateResources/css/style.css");
		list.add(0,this.webPage.baseUrl+"/templateResources/css/bootstrap.min.css");
		return list;		
	}

	override List<String> linkedScripts() {		 
		val list = BasicHtmlGenerator.super.linkedScripts;
		list.add(0,this.webPage.baseUrl+"/templateResources/js/application.js");
		list.add(0,this.webPage.baseUrl+"/templateResources/js/bootstrap.bundle.min.js");
		list.add(0,this.webPage.baseUrl+"/templateResources/js/jquery-3.7.1.min.js");
		return list;
	}
	
	def String rootNavBarContent() {
		return '''''';
	}
	
	def String additionalRootNavbarContent(){
		return '''''';
	}

	def boolean suppressNavBar(){
		return false;
	}
	
	def getLangSwitchLinks(){
		return '''''';
	}
	
	override pageTitle() {
		return '''
			<div class="row">
				<div class="col">
					<h1>�this.title�</h1>
				</div>
			</div>
		''';
	}
		
	def getPageTitleText(){
		return '''''';
	}
	override header(){
		return '''
			<div class="container bg-tu p-3">
				<div class="row">
					�this.getValueContent(this.htmlFragment.targetHeader, true)�
				</div>
			</div>
			�IF !this.suppressNavBar�
				<div class="container navcontainer">
					<nav class="navbar navbar-expand-sm navbar-dark bg-tu">
						<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target=".navbar-collapse" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse">
							<ul class="navbar-nav">
								<!-- Links -->
								�this.rootNavBarContent�
						
	���					                     �IF catalogueContext !== null && version !== null�  
	���						<li class="nav-item btn btn-outline-light">                        
	���				�""�					<a class="nav-link text-white" href="�catalogueIndex.baseUrl+catalogueContext.getQueryString(version,language)�">Catalogue �version.versionString�</a>
	���									</li>
	���							�ENDIF�
	���			                         �IF artifactContext !== null� 
	���				<li class="nav-item btn btn-outline-light">                         
	���				�""�					<a class="nav-link text-white" href="�artifactContext.getHTMLFrontendURL(artifactContext.model,version)�">Model �artifactContext.model.name�</a>
	���									</li> 
	���							�ENDIF�
							</ul>
						</div>
���						�IF catalogueIndex.enableSearch�
���							<form class="form-inline">
���								<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
���								<button class="btn btn-outline-secondary my-2 my-sm-0 mr-md-5" type="submit" disabled>Search</button>
���							</form>
���						�ENDIF�
���						�IF catalogueContext.supportedLanguages.size > 1�
���							<ul class="navbar-nav ml-auto">
���								<li class="nav-item dropdown">
���									<a class="nav-link dropdown-toggle text-white" href="#" id="navbardrop" data-toggle="dropdown">Language</a>
���									<div class="dropdown-menu dropdown-menu-right">
���										�langSwitchLinks�
���									</div>
���								</li>
���							</ul>
���						�ENDIF�
					</nav>
					�this.additionalRootNavbarContent()�
				</div>
			�ENDIF�
		''';
	}
		
	override footer(){
		return '''
			<footer>
			    <div class="container bg-tu p-3">
					<div class="row">
						�this.getValueContent(this.htmlFragment.targetFooter, true)�
					</div>
				</div>
			</footer>
		''';
	}
	
	override useNavigation () {
		return false;
	}
	
	def String getPageTitleCode () {
		if (this.htmlFragment.pageTitle !== null) {
			var String titleContent = this.htmlFragment.pageTitle.getValueContent(true);
			if (titleContent !== null && !titleContent.blank) {
				return this.stringSubstitutor.substitute(titleContent);
			}
			return "";
		}
		return '''<h1 class="page-title">�getPageTitleText�</h1>''';
	}
	
	def String getPageTitle () {
		return '''			
			�IF !getPageTitleText.blank && !getPageTitleCode.blank�
				<div class="row �IF this.useNavigation�justify-content-end�ENDIF�">
					<div class="col�IF this.useNavigation�-md-11�ENDIF�">
						�pageTitleCode�
					</div>
				</div>
			�ENDIF�
		''';
	}
		
	override String mainStructure() {
		return '''
			<div class="container">
				�this.beforeTitle�
				�getPageTitle�
				�this.belowTitle�
				
				�IF useNavigation�
					<div class="row section-padding justify-content-end">
				�ELSE�
					<div class="row section-padding justify-content-center">
				�ENDIF�		
					�IF this.useNavigation�
						�navigation�
					�ENDIF�
					�mainContent�
				</div>
			</div>
		''';
	}
	
	def String belowTitle() {
		return "";
	}
	
	def String beforeTitle() {
		return "";
	}
	
	//Helper Methods
}
