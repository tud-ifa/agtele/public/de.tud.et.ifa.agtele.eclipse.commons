/**
 */
package de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel;

import java.util.LinkedHashMap;
import java.util.Map;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Header Tag Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.HeaderTagValue#getTagType <em>Tag Type</em>}</li>
 *   <li>{@link de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.HeaderTagValue#getAttributes <em>Attributes</em>}</li>
 * </ul>
 *
 * @see de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.WebPageModelPackage#getHeaderTagValue()
 * @model
 * @generated
 */
public interface HeaderTagValue extends Value {
	/**
	 * Returns the value of the '<em><b>Tag Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag Type</em>' attribute.
	 * @see #setTagType(String)
	 * @see de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.WebPageModelPackage#getHeaderTagValue_TagType()
	 * @model required="true"
	 * @generated
	 */
	String getTagType();

	/**
	 * Sets the value of the '{@link de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.HeaderTagValue#getTagType <em>Tag Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tag Type</em>' attribute.
	 * @see #getTagType()
	 * @generated
	 */
	void setTagType(String value);

	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' map.
	 * @see de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.WebPageModelPackage#getHeaderTagValue_Attributes()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;"
	 * @generated
	 */
	EMap<String, String> getAttributes();

	public default boolean isValid() {
		if (this.getTagType() == null || this.getTagType().isBlank()) {
			return false;
		}
		return true;
	}
	public default Map<String, String> getValidAttributes () {
		Map<String, String> result = new LinkedHashMap<>();
		for (Map.Entry<String, String> e : this.getAttributes()) {
			if (e.getKey() == null || e.getKey().isBlank()) {
				continue;
			}
			if (e.getValue() == null || e.getValue().isBlank()) {
				result.put(e.getKey(), null);
			} else {
				result.put(e.getKey(), e.getValue());
			}
		}
		return result;
	}

} // HeaderTagValue
