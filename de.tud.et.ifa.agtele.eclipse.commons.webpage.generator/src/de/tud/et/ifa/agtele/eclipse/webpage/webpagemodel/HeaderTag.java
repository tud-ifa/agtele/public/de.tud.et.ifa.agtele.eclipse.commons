/**
 */
package de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Header Tag</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.WebPageModelPackage#getHeaderTag()
 * @model
 * @generated
 */
public interface HeaderTag extends AbstractKeyVal<HeaderTagValue> {

	
} // HeaderTag
