/**
 */
package de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.impl;

import de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.HeaderTag;
import de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.HeaderTagValue;
import de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.WebPageModelPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Header Tag</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class HeaderTagImpl extends AbstractKeyValImpl<HeaderTagValue> implements HeaderTag {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HeaderTagImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebPageModelPackage.Literals.HEADER_TAG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * This is specialized for the more specific element type known in this context.
	 * @generated
	 */
	@Override
	public EList<HeaderTagValue> getValue() {
		if (value == null) {
			value = new EObjectContainmentEList<HeaderTagValue>(HeaderTagValue.class, this, WebPageModelPackage.HEADER_TAG__VALUE);
		}
		return value;
	}

} //HeaderTagImpl
