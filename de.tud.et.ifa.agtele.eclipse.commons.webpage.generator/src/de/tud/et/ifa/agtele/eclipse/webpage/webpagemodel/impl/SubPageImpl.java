/**
 */
package de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.impl;

import de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.Page;
import de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.SubPage;
import de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.WebPageModelPackage;

import de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.util.WebPageModelValidator;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sub Page</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.impl.SubPageImpl#getSubPage <em>Sub Page</em>}</li>
 *   <li>{@link de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.impl.SubPageImpl#isHidden <em>Hidden</em>}</li>
 *   <li>{@link de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.impl.SubPageImpl#isSuppressNavigationMenu <em>Suppress Navigation Menu</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubPageImpl extends AbstractHTMLImpl implements SubPage {
	/**
	 * The cached value of the '{@link #getSubPage() <em>Sub Page</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubPage()
	 * @generated
	 * @ordered
	 */
	protected EList<SubPage> subPage;

	/**
	 * The default value of the '{@link #isHidden() <em>Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHidden()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HIDDEN_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isHidden() <em>Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHidden()
	 * @generated
	 * @ordered
	 */
    protected boolean hidden = HIDDEN_EDEFAULT;

	/**
	 * The default value of the '{@link #isSuppressNavigationMenu() <em>Suppress Navigation Menu</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSuppressNavigationMenu()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SUPPRESS_NAVIGATION_MENU_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSuppressNavigationMenu() <em>Suppress Navigation Menu</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSuppressNavigationMenu()
	 * @generated
	 * @ordered
	 */
    protected boolean suppressNavigationMenu = SUPPRESS_NAVIGATION_MENU_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubPageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebPageModelPackage.Literals.SUB_PAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SubPage> getSubPage() {	
	
		if (subPage == null) {
			subPage = new EObjectContainmentEList<SubPage>(SubPage.class, this, WebPageModelPackage.SUB_PAGE__SUB_PAGE);
		}
		return subPage;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isHidden() {	
	
		return hidden;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHidden(boolean newHidden) {
	
		boolean oldHidden = hidden;
		hidden = newHidden;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageModelPackage.SUB_PAGE__HIDDEN, oldHidden, hidden));
	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSuppressNavigationMenu() {	
	
		return suppressNavigationMenu;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSuppressNavigationMenu(boolean newSuppressNavigationMenu) {
	
		boolean oldSuppressNavigationMenu = suppressNavigationMenu;
		suppressNavigationMenu = newSuppressNavigationMenu;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageModelPackage.SUB_PAGE__SUPPRESS_NAVIGATION_MENU, oldSuppressNavigationMenu, suppressNavigationMenu));
	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean validateSubPage(final DiagnosticChain diagnostics, final Map<?, ?> context) {
		boolean valid = true;
		for (SubPage page : this.getSubPage()) {
			if (page instanceof Page) {
				if (diagnostics != null) {
					diagnostics.add(new BasicDiagnostic(Diagnostic.ERROR, WebPageModelValidator.DIAGNOSTIC_SOURCE,
							WebPageModelValidator.SUB_PAGE__VALIDATE_SUB_PAGE, "SubPage must only contain SubPages",
							new Object[] { this, page }));
				}
				valid = false;
			}
		}
		return valid;	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebPageModelPackage.SUB_PAGE__SUB_PAGE:
				return ((InternalEList<?>)getSubPage()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebPageModelPackage.SUB_PAGE__SUB_PAGE:
				return getSubPage();
			case WebPageModelPackage.SUB_PAGE__HIDDEN:
				return isHidden();
			case WebPageModelPackage.SUB_PAGE__SUPPRESS_NAVIGATION_MENU:
				return isSuppressNavigationMenu();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebPageModelPackage.SUB_PAGE__SUB_PAGE:
				getSubPage().clear();
				getSubPage().addAll((Collection<? extends SubPage>)newValue);
				return;
			case WebPageModelPackage.SUB_PAGE__HIDDEN:
				setHidden((Boolean)newValue);
				return;
			case WebPageModelPackage.SUB_PAGE__SUPPRESS_NAVIGATION_MENU:
				setSuppressNavigationMenu((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebPageModelPackage.SUB_PAGE__SUB_PAGE:
				getSubPage().clear();
				return;
			case WebPageModelPackage.SUB_PAGE__HIDDEN:
				setHidden(HIDDEN_EDEFAULT);
				return;
			case WebPageModelPackage.SUB_PAGE__SUPPRESS_NAVIGATION_MENU:
				setSuppressNavigationMenu(SUPPRESS_NAVIGATION_MENU_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

 	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebPageModelPackage.SUB_PAGE__SUB_PAGE:
				return subPage != null && !subPage.isEmpty();
			case WebPageModelPackage.SUB_PAGE__HIDDEN:
				return hidden != HIDDEN_EDEFAULT;
			case WebPageModelPackage.SUB_PAGE__SUPPRESS_NAVIGATION_MENU:
				return suppressNavigationMenu != SUPPRESS_NAVIGATION_MENU_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WebPageModelPackage.SUB_PAGE___VALIDATE_SUB_PAGE__DIAGNOSTICCHAIN_MAP:
				return validateSubPage((DiagnosticChain)arguments.get(0), (Map<?, ?>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (hidden: ");
		result.append(hidden);
		result.append(", suppressNavigationMenu: ");
		result.append(suppressNavigationMenu);
		result.append(')');
		return result.toString();
	}
} //SubPageImpl
