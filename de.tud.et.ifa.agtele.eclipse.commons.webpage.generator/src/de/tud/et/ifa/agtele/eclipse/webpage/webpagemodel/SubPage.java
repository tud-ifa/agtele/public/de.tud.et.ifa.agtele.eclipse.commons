/**
 */
package de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel;

import org.eclipse.emf.common.util.EList;

import de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.util.WebPageModelUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Page</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.SubPage#getSubPage <em>Sub Page</em>}</li>
 *   <li>{@link de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.SubPage#isHidden <em>Hidden</em>}</li>
 *   <li>{@link de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.SubPage#isSuppressNavigationMenu <em>Suppress Navigation Menu</em>}</li>
 * </ul>
 *
 * @see de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.WebPageModelPackage#getSubPage()
 * @model
 * @generated
 */
public interface SubPage extends AbstractHTML {
	/**
	 * Returns the value of the '<em><b>Sub Page</b></em>' containment reference list.
	 * The list contents are of type {@link de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.SubPage}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Page</em>' containment reference list.
	 * @see de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.WebPageModelPackage#getSubPage_SubPage()
	 * @model containment="true"
	 * @generated
	 */
	EList<SubPage> getSubPage();
		
	/**
	 * Returns the value of the '<em><b>Hidden</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hidden</em>' attribute.
	 * @see #setHidden(boolean)
	 * @see de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.WebPageModelPackage#getSubPage_Hidden()
	 * @model default="false"
	 * @generated
	 */
	boolean isHidden();

	/**
	 * Sets the value of the '{@link de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.SubPage#isHidden <em>Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hidden</em>' attribute.
	 * @see #isHidden()
	 * @generated
	 */
	void setHidden(boolean value);

	/**
	 * Returns the value of the '<em><b>Suppress Navigation Menu</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Navigation Menu</em>' attribute.
	 * @see #setSuppressNavigationMenu(boolean)
	 * @see de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.WebPageModelPackage#getSubPage_SuppressNavigationMenu()
	 * @model default="false"
	 * @generated
	 */
	boolean isSuppressNavigationMenu();

	/**
	 * Sets the value of the '{@link de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.SubPage#isSuppressNavigationMenu <em>Suppress Navigation Menu</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Suppress Navigation Menu</em>' attribute.
	 * @see #isSuppressNavigationMenu()
	 * @generated
	 */
	void setSuppressNavigationMenu(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='boolean valid = true;\r\nfor (&lt;%de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.SubPage%&gt; page : this.getSubPage()) {\r\n\tif (page instanceof &lt;%de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.Page%&gt;) {\r\n\t\tif (diagnostics != null) {\r\n\t\t\tdiagnostics.add(new &lt;%org.eclipse.emf.common.util.BasicDiagnostic%&gt;(&lt;%org.eclipse.emf.common.util.Diagnostic%&gt;.ERROR, &lt;%de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.util.WebPageModelValidator%&gt;.DIAGNOSTIC_SOURCE,\r\n\t\t\t\t\tWebPageModelValidator.SUB_PAGE__VALIDATE_SUB_PAGE, \"SubPage must only contain SubPages\",\r\n\t\t\t\t\tnew Object[] { this, page }));\r\n\t\t}\r\n\t\tvalid = false;\r\n\t}\r\n}\r\nreturn valid;'"
	 * @generated
	 */
	boolean validateSubPage(DiagnosticChain diagnostics, Map<?, ?> context);

	default String getSrcDir () {
		String parentDir = "",
				srcPathFragment = this.getSrcPathFragment();
		if (this.eContainer() instanceof AbstractHTML) {
			parentDir = ((AbstractHTML) this.eContainer()).getSrcDir();
		}
		if (!this.isNode()) {
			return parentDir;
		}
		if (srcPathFragment != null && !srcPathFragment.isBlank()) {
			if (WebPageModelUtils.isAbsolutePath(srcPathFragment)) {
				return srcPathFragment;
			}
		} else {
			srcPathFragment = WebPageModelUtils.getUrlSafeName(this.getName());
		}
		return (!parentDir.isBlank() ? parentDir + "/" : "") + srcPathFragment;
	}
	
	default boolean isNode () {
		return !this.getSubPage().isEmpty();
	}

	default List<SubPage> getNonHiddenSubPages () {
		return this.getSubPage().stream().filter(sp -> !sp.isHidden()).collect(Collectors.toList());
	}
} // SubPage
