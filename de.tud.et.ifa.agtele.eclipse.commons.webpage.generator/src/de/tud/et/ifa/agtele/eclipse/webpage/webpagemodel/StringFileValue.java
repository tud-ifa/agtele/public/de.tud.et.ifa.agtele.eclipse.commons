/**
 */
package de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String File Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.WebPageModelPackage#getStringFileValue()
 * @model abstract="true"
 * @generated
 */
public interface StringFileValue extends Value {
} // StringFileValue
