/**
 */
package de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.tud.et.ifa.agtele.eclipse.webpage.webpagemodel.WebPageModelPackage#getValue()
 * @model abstract="true"
 * @generated
 */
public interface Value extends EObject {
	
	default WebPage getWebPage() {
		EObject cnt = this.eContainer();
		while (cnt != null && !(cnt instanceof WebPage)) {
			cnt = cnt.eContainer();
		}
		if (cnt instanceof WebPage) {
			return (WebPage) cnt;
		}
		return null;
	}
} // Value
