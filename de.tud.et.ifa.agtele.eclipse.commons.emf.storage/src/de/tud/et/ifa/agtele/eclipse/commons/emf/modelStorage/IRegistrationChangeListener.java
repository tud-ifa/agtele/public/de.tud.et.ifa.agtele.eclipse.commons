package de.tud.et.ifa.agtele.eclipse.commons.emf.modelStorage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;

public interface IRegistrationChangeListener {
	public static enum ChangeType {
		DEREGISTERED,
		REGISTERED;
	}
		
	public void notifyChanged(RegistrationChangeNotification notification);
	
	public default Set<ModelStorage> filterByModelStorage() {
		return null;
	}
	public default Set<Model> filterByModel(){
		return null;
	}
	public default Set<EObject> filterByElement() {
		return null;
	}
	public default Set<String> filterById() {
		return null;
	}
	public default ChangeType filterByChangeType() {
		return null;
	}
	
	public static class IdentifyableElementsMap extends LinkedHashMap<String, List<EObject>> {
		private static final long serialVersionUID = -2603285189462476618L;
		
		public IdentifyableElementsMap () {};

		public void addElement (String id, List<EObject> objs) {
			if (objs == null || objs.isEmpty()) {
				return;
			}
			if (id != null && !id.isBlank()) {
				List<EObject> list = this.get(id);
				if (list == null) {
					list = new ArrayList<>();
					this.put(id, list);
				}
				for (EObject o : objs) {
					if (!list.contains(o)) {
						list.add(o);
					}
				}
			}
		}
		
		public void addElement (String id, EObject obj) {
			if (id == null || id.isBlank() || obj == null) {
				return;
			}
			List<EObject> list = this.get(id);
			if (list == null) {
				list = new ArrayList<>();
				this.put(id, list);
			}
			if (!list.contains(obj)) {
				list.add(obj);
			}
		}
		public void addElement (List<String> ids, EObject obj) {
			if (ids == null || obj == null) {
				return;
			}
			for (String id : ids) {
				this.addElement(id, obj);
			}
		}
		public List<String> getIdentifiers (EObject obj) {
			List<String> result = new ArrayList<>();
			for (Entry<String, List<EObject>> e : this.entrySet()) {
				if (e.getValue().contains(obj)) {
					result.add(e.getKey());
				}
			}
			return result;
		}
		public List<EObject> getElements () {
			List<EObject> result = new ArrayList<>();
			for (List<EObject> e : this.values()) {
				for (EObject o : e) {
					if (!result.contains(o)) {
						result.add(o);
					}
				}
			}
			return result;
		}

		public void add(IdentifyableElementsMap elements) {
			for (Entry<String, List<EObject>> e : elements.entrySet()) {
				this.addElement(e.getKey(), e.getValue());
			}
		}
		public void remove(IdentifyableElementsMap elements) {
			for (Entry<String, List<EObject>> e : elements.entrySet()) {
				List<EObject> myList = this.get(e.getKey());
				if (myList != null) {
					myList.removeAll(e.getValue());
					if (myList.isEmpty()) {
						this.remove(e.getKey());
					}
				}
			}
		}
	}
	
	public static class RegistrationChangeNotification {
		private ModelStorage storage;
		private Model model;
		private IdentifyableElementsMap elements;
		private ChangeType changeType;

		@SuppressWarnings("unused")
		private RegistrationChangeNotification() {
		}
		
		public RegistrationChangeNotification(ModelStorage storage, Model model, IdentifyableElementsMap elements, ChangeType changeType) {
			this.storage = storage;
			this.model = model;
			this.elements = elements;
			this.changeType = changeType;
		}
		
		public ModelStorage getModelStorage() {
			return this.storage;
		}
		public Model getModel() {
			return this.model;
		}
		public List<EObject> getElements() {
			return this.elements.getElements();
		}
		public Set<String> getIds() {
			return this.elements.keySet();
		}
		public ChangeType getChangeType() {
			return this.changeType;
		}
	}
}
