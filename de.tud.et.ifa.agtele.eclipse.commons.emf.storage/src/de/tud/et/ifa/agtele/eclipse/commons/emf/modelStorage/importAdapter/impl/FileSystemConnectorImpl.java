/**
 */
package de.tud.et.ifa.agtele.eclipse.commons.emf.modelStorage.importAdapter.impl;

import de.tud.et.ifa.agtele.eclipse.commons.emf.modelStorage.importAdapter.FileSystemConnector;
import de.tud.et.ifa.agtele.eclipse.commons.emf.modelStorage.importAdapter.ImportAdapter;
import de.tud.et.ifa.agtele.eclipse.commons.emf.modelStorage.importAdapter.ImportAdapterFactory;
import de.tud.et.ifa.agtele.eclipse.commons.emf.modelStorage.importAdapter.ImportAdapterPackage;
import de.tud.et.ifa.agtele.emf.AgteleEcoreUtil;
import de.tud.et.ifa.agtele.emf.importing.INodeDescriptor;
import de.tud.et.ifa.agtele.emf.importing.NodeDescriptorImpl;
import de.tud.et.ifa.agtele.resources.BundleContentHelper;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.ui.internal.Workbench;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>File System Connector</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FileSystemConnectorImpl extends ConnectorImpl implements FileSystemConnector {
	protected ResourceSetImpl resourceSet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FileSystemConnectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ImportAdapterPackage.Literals.FILE_SYSTEM_CONNECTOR;
	}
	
	protected Map<?,?> getLoadOptions() {
		HashMap<String, Object> result = new HashMap<>();
		
		return result;
	}
	
	protected boolean connected = false;
	protected Map<EObject, String> contentToResourceUriMap = new HashMap<>();
	
	@Override
	public boolean isWildCardUri() {
		String uri = this.getConnectionUri();
		return uri.contains("*") || uri.contains("?") || uri.contains("[");
	}
	
	protected void handleWildCard (String path) {
//		this.applyGlob("C:/git/de.tud.et.ifa.agtele.i40.demo.pnp", "src/plugins/casestudy-common-aas-types/data/**/*.aas").forEach(p-> {
//			System.out.println(p.toString());
//		});
		
		
		String searchPath = null, uriPrefix = null, pattern = null;
		
		//determine the absolute file path location
		if (path.startsWith("platform:/resource/")) {
			//next segment is project name
			uriPrefix = "platform:/resource/";
		} else if (path.startsWith("platform:/plugin/")) {
			//next segment is bundle id
			uriPrefix = "platform:/resource/";
		}
		if (uriPrefix == null) {
			return;
		}
			
		pattern = path.substring(uriPrefix.length());
		uriPrefix += pattern.substring(0, pattern.indexOf("/"));
		pattern = pattern.substring(pattern.indexOf("/") + 1);
		// path is the actual pattern
		
		if (uriPrefix.startsWith("platform:/plugin/")) {
			searchPath = BundleContentHelper.getBundleLocation(uriPrefix);
		} else if (uriPrefix.startsWith("platform:/resource/")) {
			IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(uriPrefix.substring("platform:/resource/".length()));
			
			searchPath = project.getRawLocation().toString();		
		}
		
		
		for (Path p : this.applyGlob(searchPath, pattern)) {
			String foundPath = p.toString().replaceAll("\\\\", "/");
			
			foundPath = foundPath.replace(searchPath, "");

			XMIResourceImpl resource = new XMIResourceImpl();	
			resource.setURI(URI.createURI(uriPrefix + foundPath));
			this.resourceSet.getResources().add(resource);			
		}
	}
	
	protected List<Path> applyGlob(String base, String pattern) {
		List<Path> result = new ArrayList<>();
		Path basePath = Paths.get(base);
		
		String[] segments = pattern.split("[/\\\\]");
		
		for (int i = 0; i < segments.length; i += 1) {
			try {
				Path testPath = Paths.get(basePath.toString(), segments[i]);
				if (Files.exists(testPath)) {
					basePath = testPath;
					pattern = pattern.substring(segments[i].length() + 1);
				} else {
					break;
				}
			} catch (InvalidPathException e) {
				break;
			}
		}
		
        Stream<Path> pStream = null;
		try {
			FileSystem fs = basePath.getFileSystem();
	        PathMatcher matcher = fs.getPathMatcher("glob:" + pattern);
	        pStream = Files.find(basePath,
	                Integer.MAX_VALUE,
	                (filePath, fileAttr) -> fileAttr.isRegularFile() && matcher.matches(filePath));
	        
	        pStream.forEach(result::add);	        
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (pStream != null) {
				pStream.close();
			}
		}
		
		return result;
	}
	
	protected boolean loadResources () {
		if (this.resourceSet.getResources().isEmpty()) {
			return false;
		}
		List<Resource> loaded = new ArrayList<>();
		this.contentToResourceUriMap.clear();
		try {
			for (Resource res : this.resourceSet.getResources()) {
					res.load(this.getLoadOptions());
					loaded.add(res);
					for (EObject content : res.getContents()) {
						this.contentToResourceUriMap.put(content, res.getURI().toString());
					}
			}
		} catch (IOException e) {
			e.printStackTrace();
			for (Resource res : loaded) {
				res.unload();
			}
			return false;
		}
		return true;
	}
	
	public String getUriByResourceContent(EObject eObj) {
		eObj = AgteleEcoreUtil.getRoot(eObj);
		if (this.contentToResourceUriMap.containsKey(eObj)) {
			return this.contentToResourceUriMap.get(eObj);
		}
		if (this.getCurrentImportAdapter() != null && this.getCurrentImportAdapter().getOriginalNode(eObj) instanceof EObject) {
			eObj = (EObject)this.getCurrentImportAdapter().getOriginalNode(eObj);
		}
		return this.contentToResourceUriMap.get(eObj);
	}
	
	@Override
	public void connect() {
		if (connected) {
			return;
		}
		this.resourceSet = new ResourceSetImpl();
		
		String uri = this.getConnectionUri().replace("platform://", "platform:/");
		
		if (this.isWildCardUri()) {
			this.handleWildCard(uri);
		} else {
			XMIResourceImpl resource = new XMIResourceImpl();	
			resource.setURI(URI.createURI(uri));
			this.resourceSet.getResources().add(resource);
		}
		if (this.loadResources()) {
			connected = true;
		}
	}

	@Override
	public void disconnect() {
		if (!connected) {
			return;
		}
		this.resourceSet.getResources().clear();
		this.resourceSet = null;	
		connected = false;
	}

	@Override
	public boolean isConnected() {
		return connected;
	}

	@Override
	public ImportAdapter createImportAdapter() {
		this.adapter = ImportAdapterFactory.eINSTANCE.createImportAdapter();
		this.adapter.setConnector(this);
		return this.adapter;
	}

	@Override
	public Collection<Object> browse(Object node) {
		ArrayList<Object> result = new ArrayList<>();
		if (node == null) {
			for (Resource res : this.resourceSet.getResources()) {
				result.addAll(res.getContents());
			}
			return result;
		}
		if (node instanceof EObject) {
			result.addAll(((EObject)node).eContents());
		}
		
		return result;
	}

	@Override
	public INodeDescriptor getTypeInfo(Object node) {
		if (node instanceof EObject) {
			EObject o = (EObject) node;
			return new NodeDescriptorImpl(node, o.eClass().getEPackage().getNsURI(), o.eClass().getName());
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Object> readReference(Object node, EReference ref) {
		ArrayList<Object> result = new ArrayList<>();				
		if (node instanceof EObject) {
			Object value = ((EObject)node).eGet(ref);
			if (value instanceof EList) {
				result.addAll((Collection<? extends Object>)value);
			} else if (value != null) {
				result.add(value);
			}			
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Object> readAttribute(Object node, EAttribute attribute) {
		ArrayList<Object> result = new ArrayList<>();				
		if (node instanceof EObject) {
			Object value = ((EObject)node).eGet(attribute);
			if (value instanceof EList) {
				result.addAll((Collection<? extends Object>)value);
			} else if (value != null) {
				result.add(value);
			}			
		}
		return result;
	}

} //FileSystemConnectorImpl
