/**
 */
package de.tud.et.ifa.agtele.eclipse.commons.emf.modelStorage.importAdapter;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>File System Connector</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.tud.et.ifa.agtele.eclipse.commons.emf.modelStorage.importAdapter.ImportAdapterPackage#getFileSystemConnector()
 * @model
 * @generated
 */
public interface FileSystemConnector extends Connector {
	public static final String[] CONNECTION_SCHEMES = {"platform"};
	
	public boolean isWildCardUri();
	
	public String getUriByResourceContent(EObject eObj);
} // FileSystemConnector
